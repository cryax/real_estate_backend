## Build docker image
sudo docker build -t backend .
## Start docker image and link to mongodb
sudo docker run -p 3030:3030 --link <mongo_container>:mongodb -ti backend
