
const firebase = require("firebase-admin");

const serviceAccount = require("./google-services.json");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://zeniiland-dev.firebaseio.com"
});

const db = firebase.database();
const ref = db.ref("zeniiland-dev");
ref.once("value", function(snapshot) {
  console.log(snapshot.val());
});

const usersRef = ref.child("users");
usersRef.set({
    juZdnUOFNlVF4awYaSOwPQYcjSl2: {
    username: "kosuke",
    email: "iphuongtt@gmail.com"
  },
  jUzSi35mDBMIHK0SbPzBzkiqI3m1: {
    username: "hoavan",
    email: "phamvanhoa0106@gmail.com"
  }
});