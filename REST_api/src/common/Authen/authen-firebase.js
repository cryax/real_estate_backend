var admin = require('firebase-admin');
var serviceAccount = require('./zeniiland-dev-serviceAccountKey.json');
var serviceAccountProd = require('./sharkland-product.json');
const errors = require('@feathersjs/errors');
var firebaseURL = 'https://zeniiland-dev.firebaseio.com';

if(process.env.NODE_ENV){
    if(process.env.NODE_ENV == "production"){
        firebaseURL = "https://sharkland-prod.firebaseio.com";
        serviceAccount = serviceAccountProd;
    }
}
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: firebaseURL
})
function validate() {
    return async context => {
        if(context.params){
            if(context.params.query){
                if(context.params.query.internal){
                    context.user = context.params.query.user;
                    return;
                }
            }
        }
        const headers = context.params.fromMiddleware;
        if (!headers.authorization){
            throw new errors.Forbidden("Forbidden");
        }
        let idToken = headers.authorization.split('Bearer ')[1];
        
        await admin.auth().verifyIdToken(idToken).then(decodedIdToken => {
            context.user = decodedIdToken;
          }).catch(error => {
            throw new errors.Forbidden(error.message);
        });
        
        };
    };

module.exports = {
    validate: validate,
    // convertResponse: convertResponse
};
