const errors = require('@feathersjs/errors');
const _ = require('lodash');
class QueryBuilder {
    constructor() {
  
      this.query = {};
    }
  
  
    buildListSubFieldMin(field, subField, min){
      
      if(subField){
        const listSubFields = subField.split(','); 
        const listSubFieldsValue = min.split(',');
        if(listSubFields.length!==listSubFieldsValue.length){
          throw 'Fields and range are not identical';
        }
        const subFieldSize = listSubFields.length;
  
        const andTempt = Object.assign([], this.query.$and); 
        const queryTemp = [];
        _.range(subFieldSize).forEach(index => {
          const subFieldItem = listSubFields[index];
          const subFieldMinValue = listSubFieldsValue[index];
          let min_value = 0;
          if(subFieldMinValue){
            min_value = parseInt(subFieldMinValue, 10);
          }
          const newField = field + '.' + subFieldItem;
          const buildFieldInRangeOr = this.buildFieldInRange(newField, [min_value], true);
  
          queryTemp.push(buildFieldInRangeOr);
        });
        // for (let index in _.range(subFieldSize)){
          
        // }
        
        andTempt.push({$or: queryTemp});
  
        this.query.$and = andTempt;
  
      }
  
      return this;
    }
  
    buildSubFieldMin(field, subField, min){
      if(subField){
        let min_value = 0;
        if(min){
          min_value = parseInt(min, 10);
        }
        
        field = field + '.' + subField;
  
        this.buildFieldInRange(field, [min_value]);
      }
      
      return this;
    }
  
    buildFieldInRange(field, range, isOr){
      let min = 0;
      if(range[0])
      {
        min = parseInt(range[0], 10);
      }
      let max = Number.MAX_SAFE_INTEGER;
      if(range[1]){
        max = parseInt(range[1], 10);
      }
  
      if(min<0 || min>max){
        throw field;
      }
      if(isOr==true){
  
        const keySubFieldInRange = {};
        keySubFieldInRange[field] = {$gte: min, $lte: max};
        return keySubFieldInRange; //descending
  
      }
      else{
        this.query[field] = {$gte: min, $lte: max};
      }
      
      
      return this;
    }
  
    buildFieldValue(field, value, checkUser=false){
      if(!checkUser){
        this.query[field] = value;
      }
      else{
        let andTempt = Object.assign([], this.query.$and);
        if(_.isEmpty(andTempt)){
          andTempt = [];
        }
        else{
          andTempt = Array.from(andTempt);
        }
        const userQueryDict = {};
        const nonUserQueryDict = {};
        userQueryDict['user_'+field] = {$eq: value, $exists: true};
        nonUserQueryDict[field] = {$eq: value};
  
        andTempt.push({$or: [userQueryDict, nonUserQueryDict]});
        this.query.$and = andTempt;
      }
      
  
      return this;
    }
    buildRegexField(field, value){
      const regexValue = new RegExp(value, 'i');
      this.query[field] = {$regex: regexValue};
    }
    buildFieldValueIn(field, value, checkUser=false){
  
      if(!checkUser){
        this.query[field] = {$in: value};
      }
      else{
        const andTempt = Object.assign([], this.query.$and);
  
        const userQueryDict = {};
        const nonUserQueryDict = {};
        userQueryDict['user_'+field] = {$in: value, $exists: true};
        nonUserQueryDict[field] = {$in: value};
        nonUserQueryDict['user_'+field] = {$exists: false};
  
        andTempt.push({$or: [userQueryDict, nonUserQueryDict]});
        this.query.$and = andTempt;
      }
  
      return this;
    }
  
  
  }

class DataBuilder {
  constructor() {
    this.data = {};
  }
  buildFieldValue(field, value){
    if(value){
      this.data[field] = value;
    }
    return this;
  }
}
class createMarkerData{
  constructor(build) {
    this.query = build.query;
  }
  static get Builder() { 
    class Builder extends DataBuilder{
      constructor(data) {
        super();
        this.data = data;
      }
      keepValidFields(validFields){
        const data = {};
        // console.log(validFields);
        Object.keys(this.data).forEach(key => {
          if(validFields.includes(key)){
            data[key] = this.data[key];
          }
        });

        this.data = data;
        return this;
      }
      buildTitle(isEdit=false){
        if (isEdit && (!this.data.title)){
          return this;
        }
        if(isEdit==false && (!this.data.title)){
          throw new errors.BadRequest('Missig title');
        }
        this.buildFieldValue(markerFields.title, this.data.title);
        return this;
      }
      buildMessage(){
        this.buildFieldValue(markerFields.message, this.data.message);
        return this;
      }
      buildRealestateType(isEdit=false){
        
        if (isEdit && (!this.data.attr_realestate_type)){
          return this;
        }
        if(isEdit==false && (!this.data.attr_realestate_type)){
          throw new errors.BadRequest('Missig realestate type');
        }
        
        this.buildFieldValue(markerFields.attRealestateType, this.data.attr_realestate_type);
        // delete this.data.attr_realestate_type;
        return this;
      }
      buildLocation(isEdit=false){
        if (isEdit && ((!this.data.lng) || (!this.data.lat))){
          return this;
        }
        if(isEdit==false && ((!this.data.lng) || (!this.data.lat))){
          // throw 'category missing lng, lat';
          throw new errors.BadRequest('Missig lng, lat');
        }
        const lng = this.data.lng;
        const lat = this.data.lat;
        this.data.location = {
          "type": "Point",
          "coordinates": [parseFloat(lng), parseFloat(lat)]
        };
        this.data.location_lng = parseFloat(lng);
        this.data.location_lat = parseFloat(lat);
        return this;
        
      }
      buildPermission(userID){

      }
      buildUpdatedDate(){
        const today = new Date();
        this.buildFieldValue(markerFields.lastUpdatedDate, today);
        return this;
      }
      buildBookmark(listBookMarks){
        if(this.data.book_mark){
          
          this.buildFieldValue(markerFields.bookMark, this.data.book_mark);
        }
        
        return this;
      }
      buildSource(sourceValue){
        this.buildFieldValue(markerFields.source, sourceValue);
        return this;
      }

      build() {
        return new createCollectionData(this);
      }
    }
  
    return Builder;
  }
      
}

class createBookmarkData{
  constructor(build) {
    this.query = build.query;
  }
  static get Builder() { 
    class Builder extends DataBuilder{
      constructor(data) {
        super();
        this.data = data;
      }
      keepValidFields(validFields){
        const data = {};
        Object.keys(this.data).forEach(key => {
          if(validFields.includes(key)){
            data[key] = this.data[key];
          }
        });

        this.data = data;
        return this;
      }
      
      buildBookmarkType(isEdit=false){
        if (isEdit && (!this.data.bookmarkType)){
          return this;
        }
        if(isEdit==false && (!this.data.bookmarkType)){
          throw new errors.BadRequest('Missig realestate type');
        }
        
        this.buildFieldValue(bookmarkFields.bookmarkType, this.data.bookmarkType);
        delete this.data.bookmarkType;
        return this;
      }
      buildLocation(isEdit=false){
        if (isEdit && ((!this.data.lng) || (!this.data.lat))){
          return this;
        }
        if(isEdit==false && ((!this.data.lng) || (!this.data.lat))){
          // throw 'category missing lng, lat';
          throw new errors.BadRequest('Missig lng, lat');
        }
        const lng = this.data.lng;
        const lat = this.data.lat;
        this.data.location = {
          "type": "Point",
          "coordinates": [parseFloat(lng), parseFloat(lat)]
        };
        this.data.location_lng = parseFloat(lng);
        this.data.location_lat = parseFloat(lat);
        return this;
        
      }
      buildPermission(userID){

      }
      buildUpdatedDate(){
        const today = new Date();
        this.buildFieldValue(bookmarkFields.lastUpdatedDate, today);
        return this;
      }
      buildSource(sourceValue){
        this.buildFieldValue(bookmarkFields.source, sourceValue);
        this.buildFieldValue(bookmarkFields.isSystem, false);
        return this;
      }

      build() {
        return new createCollectionData(this);
      }
    }
  
    return Builder;
  }
      
}

class createNoteData{
  constructor(build) {
    this.query = build.query;
  }
  static get Builder() { 
    class Builder extends DataBuilder{
      constructor(data) {
        super();
        this.data = data;
      }
      keepValidFields(validFields){
        const data = {};
        _.keys(this.data).forEach(key => {
          if(validFields.includes(key)){
            data[key] = this.data[key];
          }
        });

        this.data = data;
        return this;
      }
      buildTitle(isEdit=false){
        
        if (isEdit && (!this.data.title)){
          return this;
        }
        if(isEdit==false && (!this.data.title)){
          throw new errors.BadRequest('Missig title');
        }
        this.buildFieldValue(noteFields.title, this.data.title);
        return this;
      }
      buildContent(){
        
        this.buildFieldValue(noteFields.content, this.data.content);
        
        return this;
      }
      buildMarkerID(){
        this.buildFieldValue(noteFields.markerID, this.data.marker_id);
        return this;
      }
      buildUpdatedDate(){
        const today = new Date();
        this.buildFieldValue(noteFields.lastUpdatedDate, today);
        return this;
      }
      buildCreatedDate(){
        const today = new Date();
        this.buildFieldValue(noteFields.createdDate, today);
        return this;
      }
      buildModifiedDate(){
        const today = new Date();
        this.buildFieldValue(noteFields.modifiedDate, today);
        return this;
      }
      buildUserID(userID){
        if(userID){
          this.buildFieldValue(noteFields.userID, userID);  
        } else{
          throw new errors.NotAuthenticated('No user ID');
        }
        return this;
      }
      buildCreatedBy(userID){
        if(userID){
          this.buildFieldValue(noteFields.createdBy, userID);  
        } else{
          throw new errors.NotAuthenticated('No user ID');
        }
        return this;
      }
      buildModifiedBy(userID){
        if(userID){
          this.buildFieldValue(noteFields.modifiedBy, userID);  
        } else{
          throw new errors.NotAuthenticated('No user ID');
        }
        return this;
      }
      buildDelete(isDel){
        this.data[noteFields.isDeleted] = isDel;
        
        return this;
      }
      build() {
        return new createCollectionData(this);
      }
    }
  
    return Builder;
  }
}

class createSavedMarker{
  constructor(build) {
    this.query = build.query;
  }
  static get Builder() { 
    class Builder extends DataBuilder{
      constructor(data) {
        super();
        
        this.data = data;
      }
      keepValidFields(validFields){
        const data = {};
        
        _.keys(this.data).forEach(key => {
          if(validFields.includes(key)){
            data[key] = this.data[key];
          }
        });

        this.data = data;
        
        return this;
      }
      
      buildMarkerID(){
        if(!this.data.marker_id){
          throw new errors.BadRequest('No marker ID');
        }
        this.buildFieldValue(savedMarkerFields.markerID, this.data.marker_id);
        return this;
      }
      buildUserID(userID){
        if(userID){
          this.buildFieldValue(savedMarkerFields.userID, userID);  
        } else{
          throw new errors.NotAuthenticated('No user ID');
        }
        return this;
      }
      buildBookmarkID(){
        
        if(!this.data.bookmark_id){
          throw new errors.BadRequest('No bookmark ID');
        }
        this.buildFieldValue(savedMarkerFields.bookMarkID, this.data.bookmark_id);
        return this;
      }
      buildCreatedDate(){
        const today = new Date();
        this.buildFieldValue(savedMarkerFields.createdDate, today);
        return this;
      }
      buildModifiedDate(){
        const today = new Date();
        this.buildFieldValue(savedMarkerFields.modifiedDate, today);
        return this;
      }
      
      buildCreatedBy(userID){
        if(userID){
          this.buildFieldValue(savedMarkerFields.createdBy, userID);  
        } else{
          throw new errors.NotAuthenticated('No user ID');
        }
        return this;
      }
      buildModifiedBy(userID){
        if(userID){
          this.buildFieldValue(savedMarkerFields.modifiedBy, userID);  
        } else{
          throw new errors.NotAuthenticated('No user ID');
        }
        return this;
      }
      buildDelete(isDel){
        this.data[savedMarkerFields.isDeleted] = isDel;
        
        return this;
      }
      build() {
        return new createCollectionData(this);
      }
    }
  
    return Builder;
  }
}




const defaultRad = 5;
const defaultLng = 106.658635;
const defaultLat = 10.776689;
const minPriceM2 = 10000000;



const realestateCategories = [
	"bán",
	"",
	"mua",
	"cho thuê",
	"chuyển nhượng",
	"nhượng",
	"thuê",
	"cần tìm"
]
const realestateType = [
  "đất",
	"nhà",
	"khách sạn",
	"căn hộ",
	"nhà trọ",
	"biệt thự",
	"đẹp",
	"văn phòng",
	"dãy trọ",
	"trọ",
	"phòng trọ",
	"xưởng",
	"villa",
	"chdv",
	"hostel",
	"nha",
	"cao ốc",
	"office",
	"nhà hàng",
	"building",
	"buillding"
]

const listBookMarks = ['Quan tâm',
'Cần khảo sát',
'Đã khảo sát',
'Đang thương lượng',
'Đã mua',
'Đã bán'
]

const bookmarkFields = {
  bookmarkType: 'bookmark_type',
  lng: 'lng',
  lat: 'lat',
  createdDate: 'created_date',
  lastUpdatedDate: 'last_updated_date',
  source: 'source'
}
const markerFields = {
  title: "title",
  attRealestateType: 'attr_realestate_type',
  message: 'message',
  lng: 'lng',
  lat: 'lat',
  createdDate: 'created_date',
  lastUpdatedDate: 'last_updated_date',
  note: 'note',
  source: 'source',
  isSystem: 'is_system',
  bookMark: 'book_mark'
};
const fields ={
  trans_type : 'attr_transaction_type',
  realestate_type: 'attr_realestate_type'
};

const noteFields = {
  markerID: 'marker_id',
  userID: 'user_id',
  // title: 'title',
  content: 'content',
  createdDate: 'created_date',
  createdBy: 'created_by',
  modifiedDate: 'modified_date',
  modifiedBy: 'modified_by',
  isDeleted: 'is_deleted'
};

const savedMarkerFields = {
  markerID: 'marker_id',
  userID: 'user_id',
  bookMarkID: 'bookmark_id',
  bookMarkName: 'bookmark_name',
  isDeleted: 'is_deleted',
  createdDate: 'created_date',
  createdBy: 'created_by',
  modifiedDate: 'modified_date',
  modifiedBy: 'modified_by',
}
module.exports = {
  createMarkerData: createMarkerData,
  createBookmarkData: createBookmarkData,
  createNoteData: createNoteData,
  createSavedMarker: createSavedMarker,
  markerFields: markerFields,
  DataBuilder: DataBuilder,
  noteFields: noteFields,
  QueryBuilder: QueryBuilder,
  defaultRad: defaultRad,
  defaultLng: defaultLng,
  defaultLat: defaultLat,
  realestateCategories: realestateCategories,
  fields: fields,
  savedMarkerFields: savedMarkerFields,
  defaultMinPriceM2: minPriceM2
};