const commonLibs = require('../../common/utils');
const nodejsUtil = require('util');
const errors = require('@feathersjs/errors');
const _ = require('lodash');
class CollectionQuery{
    constructor(build) {
      this.query = build.query;
    }
    static get Builder() { 
      class Builder extends commonLibs.QueryBuilder{
        constructor(params) {
          super();
          this.paramsQuery = params.query;
        }
        buildSource(userID, sourceType="all"){
          
          let listSource = ['system', userID];
          if (sourceType=="user"){
            listSource = [userID];
          }
          if (sourceType=="system"){
            listSource = ["system"];
          }
          
          this.buildFieldValueIn(commonLibs.markerFields.source, listSource);
          return this;
        }
        buildPaging()
        {
          if(this.paramsQuery.page_size){
            const pageSize = this.paramsQuery.page_size;
            const pageIndex = this.paramsQuery.page_index;
            if(parseInt(pageSize) < 0 || parseInt(pageIndex) < 0){
              throw 'Invalid params';
            }
            this.query.$skip = parseInt(pageSize) * parseInt(pageIndex);
            this.query.$limit = parseInt(pageSize);
          }
          return this;
        }
        buildLocation(){
            let lng = this.paramsQuery.lng;
            let lat = this.paramsQuery.lat;
            let rad = this.paramsQuery.rad;
            if(!lng){
                lng = commonLibs.defaultLng;
            }
            if(!lat){
                lat = commonLibs.defaultLat;
            }
            if(!rad){
                rad = commonLibs.defaultRad;
            }
            lng = parseFloat(lng);
            lat = parseFloat(lat);
            rad = parseFloat(rad);
            this.query.location = { '$geoWithin': { '$centerSphere': [ [ lng, lat], rad/3963.2] } };
            return this;
        }
        buildBasedPermission(){
          return this;
        }
        buildTransactionType(){
          if(this.paramsQuery.trans_type){
            const type = this.paramsQuery.trans_type.split(','); 
            this.buildFieldValueIn(commonLibs.fields.trans_type, type, true);
          }
          return this;
        }
        buildRealestateType(){
          if(this.paramsQuery.realestate_type){
            const type = this.paramsQuery.realestate_type.split(','); 
            this.buildFieldValueIn(commonLibs.fields.realestate_type, type, true);
          }
          return this;
        }
        buildSort(){
          return this;
        }
        build() {
          return new CollectionQuery(this);
        }
      }
      return Builder;
    }
        
  }
function validateQuery() {
    return async context => {
        try{
        const user = context.user;
        const userID = user.user_id;
        let sourceType = "all";
        context.params.query.page_index = 0;
        context.params.query.page_size = 10000;
        context.params.query.trans_type = 'bán';
        // console.log(context.params);
        const collectionQuery = new CollectionQuery.Builder(context.params)
                                .buildSource(userID, sourceType)
                                .buildLocation()
                                .buildRealestateType()
                                .buildTransactionType()
                                .buildPaging()
                                .build();
        // db.mogi_staging.find( { location: { $geoWithin: { $centerSphere: [ [ 106.666596,10.774235], 5/3963.2] } } } ).count()
        
        context.params.query =  collectionQuery.query;//{"attr_num_street" : "18 đường"}
        console.log('AI report');
        console.log(nodejsUtil.inspect(context.params.query, false, null, true));
        }
        catch(err){
        throw err;
        }
        
        return context;
        
    };
}

class ResponseList {
    constructor(minPrice, maxPrice, minPriceM2, maxPriceM2, avgPrice, totalPosts, code) {
      this.min_price = Math.round(minPrice);
      this.max_price = Math.round(maxPrice);
      this.min_price_m2 = Math.round(minPriceM2);
      this.max_price_m2 = Math.round(maxPriceM2);
      
      this.avg_price = Math.round(avgPrice);
      this.total_posts = totalPosts;
      this.code = code;}
  }

function convertResponse() {
    return async context => {
      const result = context.result;
      let resultData = result.data;

      let minPriceList = [];
      let maxPriceList = [];
      let priceM2List = [];
      
      let countPost = 0;
      resultData.forEach(data => {
        if(data.attr_price_min && data.attr_price_min>commonLibs.defaultMinPriceM2){
          if(data.attr_price_min){
            minPriceList.push(parseFloat(data.attr_price_min));
          }
          if(data.attr_price_m2){
            priceM2List.push(parseFloat(data.attr_price_m2))
          }
          countPost +=1;
        }
          
      });

      const totalPosts = countPost;
      
      // console.log(_.sortBy(priceM2List))
      let minPrice = _.min(minPriceList);
      let maxPrice = _.max(minPriceList);

      let minPriceM2 = _.min(priceM2List);
      let maxPriceM2 = _.max(priceM2List);
      let priceM2 = _.mean(priceM2List);

      
      if (!minPrice){
        minPrice = 0;
      }
      if (!maxPrice){
        maxPrice = 0;
      }
      if (!minPriceM2){
        minPriceM2 = 0;
      }
      if (!maxPriceM2){
        maxPriceM2 = 0;
      }
      if (!priceM2){
        priceM2 = 0;
      }
      
      const response = new ResponseList(minPrice, maxPrice, minPriceM2,  maxPriceM2, priceM2, totalPosts, 200);
      context.result = response;
      return context;
    };
  }

module.exports = {
    validateQuery: validateQuery,
    convertResponse: convertResponse
};
