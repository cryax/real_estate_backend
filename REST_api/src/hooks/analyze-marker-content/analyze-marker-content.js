const commonLibs = require('../../common/utils');
const nodejsUtil = require('util');
const errors = require('@feathersjs/errors');
const _ = require('lodash');
const axios = require('axios');
const MongoClient = require('mongodb').MongoClient;
const {ObjectId} = require('mongodb');

const tagServiceLink = 'http://tag_service:5000/api/v1/real-estate-extraction';
var markerCollectionName = 'mogi_staging';
if(process.env.NODE_ENV){
  if(process.env.NODE_ENV == "production"){
    markerCollectionName = 'marker';
  }
}

async function updateMarker(context, id, field){
  const mongoUrlSplit = context.app.settings.mongodb.split('/');
  const dbName = mongoUrlSplit[mongoUrlSplit.length-1];
  const url = mongoUrlSplit[0]+'//'+mongoUrlSplit[1] + mongoUrlSplit[2] + '/'+ mongoUrlSplit[3];
  const client = await MongoClient.connect(url);
  const dbo = client.db(dbName);
  await dbo.collection(markerCollectionName).updateOne({'_id': new ObjectId(id)}, {'$set': field}, function(err, res) {
    if (err) throw err;
    console.log("One document updated");
    client.close();
  });
  return;
}


function convertResponse() {
    return async context => {
        
        const bodyFormData = context.result.message;
        
        let emptyResult = true;
        let valid_tag = {};

        //========================= CALL AXIS ===========================
        const dataTags = await axios({
          method: 'post',
          url: tagServiceLink,
          data: [bodyFormData],
          config: { headers: {'Content-Type': 'application/json' }}
          });
        
        
        //========================= CREATE TAGS ===========================
        if(dataTags.data){
          const tags = dataTags.data[0].tags;
          tags.forEach(element => {
            if(element['type']!='normal'){
              emptyResult = false;  
              valid_tag['attr_'+element['type']] = element['content']
            }
          });
        }
        
        
        //========================= UPDATE FIELD ===========================
        const id = context.id;
        const mongoUrlSplit = context.app.settings.mongodb.split('/');
        const dbName = mongoUrlSplit[mongoUrlSplit.length-1];
        const url = mongoUrlSplit[0]+'//'+mongoUrlSplit[1] + mongoUrlSplit[2] + '/'+ mongoUrlSplit[3];
        const client = await MongoClient.connect(url);
        if (!client) {
          return;
        }
        try {
          const dbo = client.db(dbName);
          let res = await dbo.collection(markerCollectionName).updateOne({'_id': new ObjectId(id)}, {'$set': valid_tag});
          
        } catch (err) {
            console.log(err);
        } finally {
            client.close();
        }
        
        //========================= QUERY DOC ===========================
        const markerQuery = {
          user: context.user,        
          internal: true
        };
        
        const marker = await context.app.service('/api/v1/post').get(context.id, {query: markerQuery});
        marker['analyzed_content'] = 0;
        
        if(!emptyResult){
          marker['analyzed_content'] = 1;
        }

        context.result = marker;
      return context;
    };
  }

module.exports = {
  convertResponse: convertResponse
};
