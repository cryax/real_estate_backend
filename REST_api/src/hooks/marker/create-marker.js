const commonLibs = require('../../common/utils');

const listValidField = Object.values(commonLibs.markerFields);
function validateMarker(){
    return async context => {
        const user = context.user;
        const userId = user.user_id;
        // console.log(listValidField);
        const collectionData = new commonLibs.createMarkerData.Builder(context.data)
                                    .keepValidFields(listValidField)
                                    .buildTitle()
                                    .buildMessage()
                                    .buildLocation()
                                    .buildRealestateType()
                                    .buildUpdatedDate()
                                    .buildSource(userId);
    context.data = collectionData.data;
    // console.log(context.data);

    return context;
    };
}
function convertResponse() {
    return async context => {
        context.statusCode = 200;
      return context;
    };
  }
module.exports = {
    validateMarker: validateMarker,
    convertResponse: convertResponse
  };