const commonLibs = require('../../common/utils');

const listValidField = Object.values(commonLibs.markerFields);
function validateMarker(){
    return async context => {
        const user = context.user;
        const userId = user.user_id;
        const isEdit = true;
        // const contextData = context.data;
        const listBookMark = await context.app.service('/api/v1/book-mark').find({
            query: {
            }
          });
        
        const markerData = new commonLibs.createMarkerData.Builder(context.data)
                                    .keepValidFields(listValidField)
                                    .buildTitle(isEdit)
                                    .buildMessage()
                                    .buildLocation(isEdit)
                                    .buildRealestateType(isEdit)
                                    .buildUpdatedDate()
                                    .buildBookmark(listBookMark)
                                    .buildSource(userId);
                                    
        // console.log(collectionData.data);
    context.data = markerData.data;

    return context;
    };
}
module.exports = {
    validateMarker: validateMarker
  };