const commonLibs = require('../../common/utils');
const nodejsUtil = require('util');
const errors = require('@feathersjs/errors');
const _ = require('lodash');
class CollectionQuery{
    constructor(build) {
      this.query = build.query;
    }
    static get Builder() { 
      class Builder extends commonLibs.QueryBuilder{
        constructor(params) {
          super();
          this.paramsQuery = params.query;
          this.aggregate = [];
          this.aggregateMatch = {};
        }
        buildSource(userID, sourceType="all"){
          
          let listSource = ['system', userID];
          if (sourceType=="user"){
            listSource = [userID];
          }
          if (sourceType=="system"){
            listSource = ["system"];
          }
          
          this.buildFieldValueIn(commonLibs.markerFields.source, listSource);
          return this;
        }
        buildSelect(keepFields){
          this.query.$select = keepFields;
          return this;
        }
        buildPaging()
        {
          console.log(this.paramsQuery.page_size);
          console.log(this.paramsQuery.page_index);
          if(this.paramsQuery.page_size && this.paramsQuery.page_index){
            const pageSize = this.paramsQuery.page_size;
            const pageIndex = this.paramsQuery.page_index;
            if(parseInt(pageSize) < 0 || parseInt(pageIndex) < 0){
              throw 'Invalid params';
            }
            this.query.$skip = parseInt(pageSize) * parseInt(pageIndex);
            this.query.$limit = parseInt(pageSize);

          }
          return this;
        }
        buildListIds(){
          if(this.paramsQuery._id){
            this.buildFieldValueIn('_id', this.paramsQuery._id, false);
          }
          return this;
        }
        buildLocation(){
            if(this.paramsQuery.ignore_location){
              return this;
            }
            let lng = this.paramsQuery.lng;
            let lat = this.paramsQuery.lat;
            let rad = this.paramsQuery.rad;
            if(!lng){
                lng = commonLibs.defaultLng;
            }
            if(!lat){
                lat = commonLibs.defaultLat;
            }
            if(!rad){
                rad = commonLibs.defaultRad;
            }
            lng = parseFloat(lng);
            lat = parseFloat(lat);
            rad = parseFloat(rad);
            this.query.location = { '$geoWithin': { '$centerSphere': [ [ lng, lat], rad/3963.2] } };
            
            return this;
        }
        buildBasedPermission(){
          return this;
        }
        buildTransactionType(){
          if(this.paramsQuery.trans_type){
            const type = this.paramsQuery.trans_type.split(','); 
            this.buildFieldValueIn(commonLibs.fields.trans_type, type, true);
          }
          return this;
        }
        buildRealestateType(){
          if(this.paramsQuery.realestate_type){
            const type = this.paramsQuery.realestate_type.split(','); 
            this.buildFieldValueIn(commonLibs.fields.realestate_type, type, true);
          }
          return this;
        }
        buildAggregate(){
          const matchCond = JSON.parse(JSON.stringify(this.query));
          const lng = matchCond.location['$geoWithin']['$centerSphere'][0][0];
          const lat = matchCond.location['$geoWithin']['$centerSphere'][0][1];
          const rad = matchCond.location['$geoWithin']['$centerSphere'][1];
          
          const selectFields = matchCond['$select'];
          const projectFields = {};

          selectFields.forEach(element => {
            projectFields[element] = 1;
          });

          const skip = matchCond['$skip'];
          const limit = matchCond['$limit'];

          delete matchCond.location;
          delete matchCond['$select'];
          delete matchCond['$skip'];
          delete matchCond['$limit'];
          
          let geoNear = 
          {
            $geoNear: {
               near: { type: "Point", coordinates: [ lng , lat ] },
               distanceField: "dist.calculated",
               maxDistance: rad*1609*3963.2,
               minDistance: 0,
               includeLocs: "dist.location",
               num: 999999,
               spherical: true
            }
          };
          projectFields['dist.calculated'] = 1;
          let project = {
            $project: projectFields
          }
          
          // this.aggregate.push({$match: matchCond});
          this.aggregate.push(geoNear);
          this.aggregate.push(project)
          this.aggregateMatch = [...this.aggregate];
          this.aggregateMatch.push({$group: { _id: null, count: { $sum: 1 } }});
          this.aggregate.push({$skip: skip});
          this.aggregate.push({$limit: limit});
          return this;
        }
        buildSort(){
          return this;
        }
        build() {
          return new CollectionQuery(this);
        }
      }
      return Builder;
    }
        
}


function validateQuery() {
    return async context => {
        try{
        const user = context.user;
        const userID = user.user_id;
        let sourceType = "all";
        
        if(context.params.query.source_type){
          sourceType = context.params.query.source_type;
        }
        
        // console.log(context.params);
        const validFields = ['_id', 'location_lat', 'location_lng', 'title', 'attr_price', 'attr_area', 'source'];
        const collectionQuery = new CollectionQuery.Builder(context.params)
                                .buildSource(userID, sourceType)
                                .buildLocation()
                                .buildListIds()
                                .buildRealestateType()
                                .buildTransactionType()
                                .buildSelect(validFields)
                                .buildPaging()
                                .buildAggregate()
                                // .build();

        
        const queryData = await context.service.Model.aggregate(collectionQuery.aggregate).toArray(); 
        const totalResultAggregate = await context.service.Model.aggregate(collectionQuery.aggregateMatch).toArray(); 

        context.temp_data = queryData;
        

        if(totalResultAggregate.length==0){
          context.temp_total_aggregate = 0;  
        }
        else{
          context.temp_total_aggregate = totalResultAggregate[0].count;
          
        }
        // console.log(nodejsUtil.inspect(collectionQuery.aggregate, false, null, true));
        // console.log(nodejsUtil.inspect(context.params.query, false, null, true));
        // console.log(nodejsUtil.inspect(queryData, false, null, true));

        }
        catch(err){
        throw err;
        }
        
        return context;
        
    };
}
function markupData(item){
  const markUpObject = {};

  let lng = commonLibs.defaultLng; 
  if(item.location_lng){
    lng = item.location_lng;
  }
  
  let lat = commonLibs.defaultLat; 
  if(item.location_lng){
    lat = item.location_lat;
  }

  markUpObject['geometry'] = {"coordinates": [lng, lat]};
  markUpObject['properties'] = {
    "_id": item._id,
    "title": item.title,
    "area": item.attr_area,
    "price": item.attr_price,
    "source": item.source,
    "bookmark_id": item.bookmark_id,
    "distance": item.dist.calculated
  };
  return markUpObject;
}
function convertResponse() {
  return async context => {
    const tempData = context.temp_data;
    const totalResultAggregate = context.temp_total_aggregate;
    const result = context.result;
    let resultData = tempData;
    
    const listMarkerID = _.map(resultData, '_id');
    const listMarkerIDStr = listMarkerID.map(e=>e.toString());
    const markerQuery = {
      user: context.user,
      marker_id: listMarkerIDStr,
      internal: true,
    };
    
    const savedMarker = await context.app.service('/api/v1/saved-marker').find({
      query: markerQuery,
    })
    const savedMarkerData = savedMarker.data;
    
    const idsavedMarkerData = _.keyBy(savedMarkerData, 'marker_id');
    
    let listResult = [];
    // const validFields = ['_id', 'location_lat', 'location_lng', 'title', 'attr_price', 'attr_area', 'source'];
    resultData.forEach(element => {
      // element = keepFields(element, validFields);
      
      let bookmarkID = NaN;
      if(idsavedMarkerData[element._id.toString()]){
        // newObj['bookmark_id'] = idsavedMarkerData[element._id.toString()].bookmark_id;
        bookmarkID = idsavedMarkerData[element._id.toString()].bookmark_id;
      }
      let newObj = Object.assign(element, {'bookmark_id': bookmarkID})

      newObj = markupData(newObj);
      listResult.push(newObj);
    });

    context.result.data = listResult;
    context.result.total = totalResultAggregate;
    // context.result = result;
    return context;
  };
}
module.exports = {
    validateQuery: validateQuery,
    convertResponse: convertResponse
};