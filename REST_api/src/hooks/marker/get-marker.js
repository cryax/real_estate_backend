const commonLibs = require('../../common/utils');
const nodejsUtil = require('util');
const errors = require('@feathersjs/errors');
const {ObjectID} = require('mongodb');
const _ = require('lodash');

function validateQuery() {
    return async context => {
        try{
            context.params.query = {};
        }
        catch(err){
        throw err;
        }
        
        return context;
        
    };
}
function convertResponse() {
  return async context => {
    let result = context.result;
    const markerID = result._id.toString();

    const markerQuery = {
        user: context.user,        
        internal: true,
        marker_id: [markerID]
      };
    
    const savedMarker = await context.app.service('/api/v1/saved-marker').find({
        query: markerQuery,
      })

    if(savedMarker.data){
        if(savedMarker.data[0]){
            if(savedMarker.data[0].bookmark_id){
                result.bookmark_id = savedMarker.data[0].bookmark_id;
            }
        }
    }
    
    context.result = result;
    return context;
  };
}

module.exports = {
    validateQuery: validateQuery,
    convertResponse: convertResponse
};