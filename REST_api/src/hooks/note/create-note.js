const commonLibs = require('../../common/utils');
// const util = require('util');

const listValidField = Object.values(commonLibs.noteFields);
function validateQuery(){
    return async context => {
        const user = context.user;
        const userId = user.user_id;
        
        const collectionData = new commonLibs.createNoteData.Builder(context.data)
                                    .keepValidFields(listValidField)
                                    // .buildTitle()
                                    .buildContent()
                                    .buildMarkerID()
                                    .buildUserID(userId)
                                    .buildCreatedDate()
                                    .buildModifiedDate()
                                    .buildModifiedBy(userId)
                                    .buildDelete(0);
    context.data = collectionData.data;
    // console.log(context.data);

    return context;
    };
}

function convertResponse() {
    return async context => {
        context.statusCode = 200;
      return context;
    };
  }

module.exports = {
    validateQuery: validateQuery,
    convertResponse: convertResponse
  };