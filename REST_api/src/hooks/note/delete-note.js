const commonLibs = require('../../common/utils');
// const util = require('util');

const listValidField = Object.values(commonLibs.noteFields);
function validateQuery(){
    return async context => {
        const user = context.user;
        const userId = user.user_id;
        
        const collectionData = new commonLibs.createNoteData.Builder(context.data)
                                    .keepValidFields(listValidField)
                                    .buildUserID(userId)
                                    .buildCreatedDate()
                                    .buildModifiedDate()
                                    .buildModifiedBy(userId)
                                    .buildDelete(1);
    context.data = collectionData.data;
    

    return context;
    };
}

module.exports = {
    validateQuery: validateQuery,
    
  };