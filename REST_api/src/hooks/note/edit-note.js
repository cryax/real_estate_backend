const commonLibs = require('../../common/utils');
// const util = require('util');
const errors = require('@feathersjs/errors');
const listValidField = Object.values(commonLibs.noteFields);
function validateQuery(){
    return async context => {
        const user = context.user;
        const userId = user.user_id;
        
        const collectionData = new commonLibs.createNoteData.Builder(context.data)
                                    .keepValidFields(listValidField)
                                    // .buildTitle(isEdit=true)
                                    .buildContent()
                                    .buildModifiedDate()
                                    .buildModifiedBy(userId);
    context.data = collectionData.data;
    // console.log(context.data);

    return context;
    };
}

// function convertResponse() {
//     return async context => {
//         console.log(context.result);
//       if(context.result.user_id!=context.user.user_id){
//         throw new errors.Forbidden('You arent allowed to modify this collection');
//       }

//       return context;
//     };
//   }
module.exports = {
    validateQuery: validateQuery,
    // convertResponse: convertResponse
  };