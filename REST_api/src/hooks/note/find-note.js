const commonLibs = require('../../common/utils');
const nodejsUtil = require('util');
const errors = require('@feathersjs/errors');
const {ObjectId} = require('mongodb');

class NoteQuerry{
    constructor(build) {
      this.query = build.query;
    }
    static get Builder() { 
      class Builder extends commonLibs.QueryBuilder{
        constructor(params) {
          super();
          this.paramsQuery = params.query;
          this.aggregate = [];
          this.aggregateMatch = {};
        }
        buildUserID(userID, sourceType="user"){
          
          let listSource = ['system', userID];
          if (sourceType=="user"){
            listSource = [userID];
          }
          
          this.buildFieldValueIn(commonLibs.noteFields.userID, listSource);
          return this;
        }
        buildMarkerID(){
            
            if(this.paramsQuery.marker_id){
                this.buildFieldValue(commonLibs.noteFields.markerID, this.paramsQuery.marker_id);
            }
            return this;
        }
        buildPaging()
        {
          if(this.paramsQuery.page_size && this.paramsQuery.page_index){
            const pageSize = this.paramsQuery.page_size;
            const pageIndex = this.paramsQuery.page_index;
            if(parseInt(pageSize) < 0 || parseInt(pageIndex) < 0){
              throw 'Invalid params';
            }
            this.query.$skip = parseInt(pageSize) * parseInt(pageIndex);
            this.query.$limit = parseInt(pageSize);
          }
          return this;
        }
        buildIsNotDeleted(){
            this.buildFieldValue(commonLibs.noteFields.isDeleted, 0);
            return this;
        }
        buildBasedPermission(){
          return this;
        }
        build() {
          return new NoteQuerry(this);
        }
      }
      return Builder;
    }
        
  }
function validateQuery() {
    return async context => {
        try{
        const user = context.user;
        const userID = user.user_id;

        const noteQuerry = new NoteQuerry.Builder(context.params)
                                .buildUserID(userID)
                                .buildIsNotDeleted()
                                .buildMarkerID()
                                .buildPaging()
                                .build();
   
        context.params.query =  noteQuerry.query;//{"attr_num_street" : "18 đường"}
        
        }
        catch(err){
        throw err;
        }
        
        return context;
        
    };
}
module.exports = {
    validateQuery: validateQuery,
};