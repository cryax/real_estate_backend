const commonLibs = require('../../common/utils');
// const util = require('util');

const listValidField = Object.values(commonLibs.savedMarkerFields);
function validateQuery(){
    return async context => {
        const user = context.user;
        const userId = user.user_id;
        
        // const listBookMarks = await context.app.service('/api/v1/book-mark').find({
        //     query: {},
        //   });
        
        const collectionData = new commonLibs.createSavedMarker.Builder(context.data)
                                    .keepValidFields(listValidField)
                                    .buildMarkerID()
                                    .buildBookmarkID()
                                    .buildUserID(userId)
                                    .buildCreatedDate()
                                    .buildModifiedDate()
                                    .buildModifiedBy(userId)
                                    .buildDelete(0);
    context.data = collectionData.data;
    // console.log(context.data);

    return context;
    };
}
function convertResponse() {
    return async context => {
        context.statusCode = 200;
      return context;
    };
  }
module.exports = {
    validateQuery: validateQuery,
    convertResponse: convertResponse
  };