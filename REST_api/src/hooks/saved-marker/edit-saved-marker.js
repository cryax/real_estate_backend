const commonLibs = require('../../common/utils');

const listValidField = Object.values(commonLibs.savedMarkerFields);
function validateMarker(){
    return async context => {
        const user = context.user;
        const userId = user.user_id;
        const isEdit = true;
        // const contextData = context.data;
        
        const collectionData = new commonLibs.createSavedMarker.Builder(context.data)
                                    .keepValidFields(listValidField)
                                    .buildBookmarkID()
                                    .buildModifiedDate()
                                    .buildModifiedBy(userId)
                                    
        // console.log(collectionData.data);
    context.data = collectionData.data;

    return context;
    };
}
module.exports = {
    validateMarker: validateMarker
  };