const commonLibs = require('../../common/utils');
const nodejsUtil = require('util');
const errors = require('@feathersjs/errors');
const MongoClient = require('mongodb').MongoClient;
const {ObjectId} = require('mongodb');
const _ = require('lodash');

var markerCollectionName = 'mogi_staging';
if(process.env.NODE_ENV){
  if(process.env.NODE_ENV == "production"){
    markerCollectionName = 'marker';
  }
}

class SaveMarkerQuery{
    constructor(build) {
      this.query = build.query;
      this.aggregate = build.aggregate;
      this.aggregateMatch = build.aggregateMatch;
    }
    static get Builder() { 
      class Builder extends commonLibs.QueryBuilder{
        constructor(params) {
          super();
          this.paramsQuery = params.query;
          this.aggregate = [];
          this.aggregateMatch = {};
        }
        buildUserID(userID, sourceType="user"){
          
          let listSource = ['system', userID];
          if (sourceType=="user"){
            listSource = [userID];
          }
          
          this.buildFieldValueIn(commonLibs.savedMarkerFields.userID, listSource);
          return this;
        }
        buildMarkerID(){
            
            if(this.paramsQuery.marker_id){
                // const listMarkerID = this.paramsQuery.marker_id.split(',');
                this.buildFieldValueIn(commonLibs.savedMarkerFields.markerID, this.paramsQuery.marker_id);
            }
            return this;
        }
        buildPaging()
        {
          this.query.$skip = 0;
          this.query.$limit = 10000;
          if(this.paramsQuery.page_size && this.paramsQuery.page_index){
            const pageSize = this.paramsQuery.page_size;
            const pageIndex = this.paramsQuery.page_index;
            if(parseInt(pageSize) < 0 || parseInt(pageIndex) < 0){
              throw 'Invalid params';
            }
            this.query.$skip = parseInt(pageSize) * parseInt(pageIndex);
            this.query.$limit = parseInt(pageSize);
          }
          return this;
        }
        buildIsNotDeleted(){
            this.buildFieldValue(commonLibs.savedMarkerFields.isDeleted, 0);
            return this;
        }
        buildBookMarkID(){
          if(this.paramsQuery.bookmark_name){
            this.buildFieldValue(commonLibs.savedMarkerFields.bookMarkID, this.paramsQuery.bookmark_id);
          }
          return this;
          
        }
        buildBasedPermission(){
          return this;
        }
        buildSort(){

          let sortStrategy = -1;
          if(this.paramsQuery.sort_type){
            sortStrategy = parseInt(this.paramsQuery.sort_type);
          }
          const sortField = 'created_date';
          const sortDict = {};
          sortDict[sortField] = sortStrategy;
          this.query.$sort = sortDict; //descending
          return this;
        }
        buildAggregate(){
          const sortAggregate = {$sort: {created_date:-1}};
          this.aggregate.push(sortAggregate);

          
          const matchCond = Object.assign({}, this.query);
          
          delete matchCond.$skip;
          delete matchCond.$limit;
          delete matchCond.$sort;
          
          this.aggregate.push({$match: matchCond});
          //FIELDS to keep
          this.aggregate.push({
            "$project": { 
              "marker_id_obj": {"$toObjectId": "$marker_id"},
              "marker_id": "$marker_id",
              "bookmark_id": "$bookmark_id",       
              "user_id": "$user_id",       
              "created_date": "$created_date",       
              "modified_date": "$modified_date",       
              "modified_by": "$modified_by",
            }});
          //JOIN table operation
          this.aggregate.push({
            $lookup:
              {
                from: markerCollectionName,
                localField: "marker_id_obj",
                foreignField: "_id",
                as: "inventory_docs"
              }
          },
          {
            $match:
            {
                inventory_docs: {$not: {$size: 0}}
            }
          },
          );
          
          this.aggregateMatch = [...this.aggregate];
          this.aggregateMatch.push({$group: { _id: null, count: { $sum: 1 } }});
          this.aggregate.push({$skip: this.query.$skip});
          this.aggregate.push({$limit: this.query.$limit});
          

          return this;
        }
        build() {
          return new SaveMarkerQuery(this);
        }
      }
      return Builder;
    }
        
  }
function validateQuery() {
    return async context => {
        try{
        const user = context.user;
        const userID = user.user_id;

        const noteQuerry = new SaveMarkerQuery.Builder(context.params)
                                .buildUserID(userID)
                                // .buildIsNotDeleted()
                                .buildMarkerID()
                                .buildBookMarkID()
                                .buildPaging()
                                .buildSort()
                                .buildAggregate()
                                .build();
        
        context.params.query =  noteQuerry.query;//{"attr_num_street" : "18 đường"}

        
        const queryData = await context.service.Model.aggregate(noteQuerry.aggregate).toArray(); 

        const totalResultAggregate = await context.service.Model.aggregate(noteQuerry.aggregateMatch).toArray(); 


        context.temp_data = queryData;
        

        if(totalResultAggregate.length==0){
          context.temp_total_aggregate = 0;  
        }
        else{
          context.temp_total_aggregate = totalResultAggregate[0].count;
          
        }
        // console.log('noteQuerry');
        // console.log(nodejsUtil.inspect(context.params.query, false, null, true));
        }
        catch(err){
        throw err;
        }
        
        return context;
        
    };
}

function convertResponse() {
  return async context => {
    const tempData = context.temp_data;
    
    const totalResultAggregate = context.temp_total_aggregate;


    const newResultData = [];
    tempData.forEach(element => {
      inventory_docs = element.inventory_docs;
      if(inventory_docs[0]){
        let title = '';
        if(inventory_docs[0].title){
          title = inventory_docs[0].title;
        }
  
        let message = '';
        if(inventory_docs[0].message){
          message = inventory_docs[0].message;
        }
  
  
        element.marker_title = title;
        element.marker_content = message;
        
        let attrNumbStreet = '';
        if(inventory_docs[0].attr_num_street){
          attrNumbStreet = inventory_docs[0].attr_num_street + ' ';
        }
  
        let attrStreet = '';
        if(inventory_docs[0].attr_addr_street){
          attrStreet = inventory_docs[0].attr_addr_street + ' ';
        }
  
        let attrWard = '';
        if(inventory_docs[0].attr_addr_ward){
          attrWard = ', ' + inventory_docs[0].attr_addr_ward + ' ';
        }
  
        let attrDistrict = '';
        if(inventory_docs[0].attr_addr_district){
          attrDistrict = ', ' + inventory_docs[0].attr_addr_district + ' ';
        }
  
        let attrCity = '';
        if(inventory_docs[0].attr_addr_city){
          attrCity = inventory_docs[0].attr_addr_city + ' ';
        }
        element.marker_addr = attrNumbStreet+
                              attrStreet+
                              attrWard+
                              attrDistrict+
                              attrCity;
        
        let attrLat = '';
        if(inventory_docs[0].location_lat){
          attrLat = inventory_docs[0].location_lat;
        }
        element.marker_location_lat = attrLat;
        let attrLng = '';
        if(inventory_docs[0].location_lng){
          attrLng = inventory_docs[0].location_lng;
        }
        element.marker_location_lng = attrLng;
        delete element.inventory_docs;
        newResultData.push(element);
      }
      
    });
    // console.log(context.result);
    // console.log(totalResultAggregate);
    context.result.total = totalResultAggregate;
    context.result.data = newResultData;
    // context.result = listMarker;
    // context.result = result;
    return context;
  };
}

module.exports = {
    validateQuery: validateQuery,
    convertResponse: convertResponse
};