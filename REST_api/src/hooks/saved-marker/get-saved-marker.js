const commonLibs = require('../../common/utils');
const nodejsUtil = require('util');
const errors = require('@feathersjs/errors');
const {ObjectID} = require('mongodb');
const _ = require('lodash');


function convertResponse() {
  return async context => {
    
    const result = context.result;
    const user = context.user;
    const markerID = result.marker_id;
    const bookmarkID = result.bookmark_id;

    const marker = await context.app.service('/api/v1/post').get(markerID.toString());
    const listBookMarks = await context.app.service('/api/v1/book-mark').find({
            query: {},
          });
    
    const listBookMarksNormed = listBookMarks.data.map(bookmark => ({...bookmark, norm_id: bookmark._id.toString()}))
    const listBookMarksNormedKey = _.keyBy(listBookMarksNormed, 'norm_id')
    
    const finalMarker = Object.assign(marker, {'bookmark_name': listBookMarksNormedKey[bookmarkID].name})


    context.result = finalMarker;
    // context.result = result;
    return context;
  };
}

module.exports = {
    
    convertResponse: convertResponse
};