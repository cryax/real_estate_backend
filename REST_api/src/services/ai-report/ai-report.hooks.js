const aiReport = require('../../hooks/ai-report/find-marker-report');

const authen = require('../../common/Authen/authen-firebase');

module.exports = {
  before: {
    all: [authen.validate()],
    find: [aiReport.validateQuery()],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [aiReport.convertResponse()],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
