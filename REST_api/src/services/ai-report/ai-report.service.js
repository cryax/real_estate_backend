// Initializes the `ai-report` service on path `/ai-report`
const createService = require('feathers-mongodb');
const hooks = require('./ai-report.hooks');
const bodyParser = require('body-parser');
const express = require('@feathersjs/express');

module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = { paginate, whitelist: [ '$geoWithin', '$centerSphere', '$and', '$insertOne'] };

  app.configure(express.rest())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: true}))
  .use(function(req, res, next) {
    req.feathers.fromMiddleware = req.headers;
    next();
  });  
  // Initialize our service with any options it requires
  app.use('/api/v1/ai-report', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/v1/ai-report');

  let colName = 'mogi_staging';
  if(app.get('env')){
    if(app.get('env') == "production"){
      colName = "marker"; 
    }
  }
  
  mongoClient.then(db => {
    service.Model = db.collection(colName);
  });

  service.hooks(hooks);
};
