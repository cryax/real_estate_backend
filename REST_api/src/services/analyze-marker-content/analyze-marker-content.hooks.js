const authen = require('../../common/Authen/authen-firebase');
const getAnalyze = require('../../hooks/analyze-marker-content/analyze-marker-content');
module.exports = {
  before: {
    all: [authen.validate()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [getAnalyze.convertResponse()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
