// Initializes the `bookmark` service on path `/bookmark`
const createService = require('feathers-mongodb');
const hooks = require('./bookmark.hooks');

module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = { paginate };

  // Initialize our service with any options it requires
  app.use('/api/v1/book-mark', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/v1/book-mark');

  
  mongoClient.then(db => {
    service.Model = db.collection('bookmark');
  });

  service.hooks(hooks);
};
