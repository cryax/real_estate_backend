const post = require('./marker/post.service.js');
const note = require('./note/note.service.js');
const aiReport = require('./ai-report/ai-report.service.js');

const bookmark = require('./bookmark/bookmark.service.js');

const savedMarker = require('./saved_marker/saved_marker.service.js');

const analyzeMarkerContent = require('./analyze-marker-content/analyze-marker-content.service.js');

// const listBookmark = require('./list-bookmark/list-bookmark.service.js');

// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(post);
  app.configure(note);

  app.configure(aiReport);
  app.configure(bookmark);
  // app.configure(listBookmark);
  app.configure(savedMarker);
  app.configure(analyzeMarkerContent);
};
