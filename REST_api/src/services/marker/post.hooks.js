const findPosts = require('../../hooks/marker/find-marker');
const createMarker = require('../../hooks/marker/create-marker');
const editMarker = require('../../hooks/marker/edit-marker');
const getMarker = require('../../hooks/marker/get-marker');
const authen = require('../../common/Authen/authen-firebase');
module.exports = {
  before: {
    all: [authen.validate()],
    find: [findPosts.validateQuery()],
    get: [getMarker.validateQuery()],
    create: [createMarker.validateMarker()],
    update: [],
    patch: [editMarker.validateMarker()],
    remove: []
  },

  after: {
    all: [],
    find: [findPosts.convertResponse()],
    get: [getMarker.convertResponse()],
    create: [createMarker.convertResponse()],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
