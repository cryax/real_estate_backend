const createService = require('feathers-mongodb');
const hooks = require('./post.hooks');
const bodyParser = require('body-parser');
const express = require('@feathersjs/express');
module.exports = function (app) {

  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  
  
  const options = { paginate, whitelist: [ '$geoWithin', '$centerSphere', '$and', '$insertOne',
  '$nearSphere', '$geometry', '$minDistance', '$maxDistance'] };

  app.configure(express.rest())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: true}))
  .use(function(req, res, next) {
    req.feathers.fromMiddleware = req.headers;
    next();
  });
  // Initialize our service with any options it requires
  app.use('/api/v1/post', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('/api/v1/post');

  let colName = 'mogi_staging';
  if(app.get('env')){
    if(app.get('env') == "production"){
      colName = "marker"; 
    }
  }

  mongoClient.then(db => {
    service.Model = db.collection(colName);
  });

  service.hooks(hooks);
};
