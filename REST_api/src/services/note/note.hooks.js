const createNote = require('../../hooks/note/create-note');
const editNote = require('../../hooks/note/edit-note');
const findNote = require('../../hooks/note/find-note');
const delNote = require('../../hooks/note/delete-note');
const authen = require('../../common/Authen/authen-firebase');
module.exports = {
  before: {
    all: [authen.validate()],
    find: [findNote.validateQuery()],
    get: [],
    create: [createNote.validateQuery()],
    update: [],
    patch: [editNote.validateQuery()],
    remove: []//delNote.validateQuery()]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [createNote.convertResponse()],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
