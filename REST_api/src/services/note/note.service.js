// Initializes the `note` service on path `/note`
const createService = require('feathers-mongodb');
const hooks = require('./note.hooks');
const bodyParser = require('body-parser');
const express = require('@feathersjs/express');
module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = { paginate, whitelist: [ '$geoWithin', '$centerSphere', '$and', '$insertOne',
  '$nearSphere', '$geometry', '$minDistance', '$maxDistance'] };

  app.configure(express.rest())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: true}))
  .use(function(req, res, next) {
    req.feathers.fromMiddleware = req.headers;
    next();
  });
  // Initialize our service with any options it requires
  app.use('api/v1/note', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/v1/note');

  let colName = 'note';
  if(app.get('env')){
    if(app.get('env') == "production"){
      colName = "note"; 
    }
  }

  mongoClient.then(db => {
    service.Model = db.collection('note');
  });

  service.hooks(hooks);
};
