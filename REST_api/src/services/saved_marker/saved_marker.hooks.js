
const authen = require('../../common/Authen/authen-firebase');
const createSavedMarker = require('../../hooks/saved-marker/create-saved-marker');
const findSavedMarker = require('../../hooks/saved-marker/find-saved-marker');
const getSavedMarker = require('../../hooks/saved-marker/get-saved-marker');
const editSavedMarker = require('../../hooks/saved-marker/edit-saved-marker');
module.exports = {
  before: {
    all: [authen.validate()],
    find: [findSavedMarker.validateQuery()],
    get: [],
    create: [createSavedMarker.validateQuery()],
    update: [],
    patch: [editSavedMarker.validateMarker()],
    remove: []
  },

  after: {
    all: [],
    find: [findSavedMarker.convertResponse()],//findSavedMarker.convertResponse()],
    get: [],//getSavedMarker.convertResponse()],
    create: [createSavedMarker.convertResponse()],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};


