// Initializes the `saved_marker` service on path `/saved-marker`
const createService = require('feathers-mongodb');
const hooks = require('./saved_marker.hooks');
const bodyParser = require('body-parser');
const express = require('@feathersjs/express');
module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  
  const options = { paginate, whitelist: [ '$geoWithin', '$centerSphere', '$and', '$insertOne',
  '$nearSphere', '$geometry', '$minDistance', '$maxDistance'] };

  app.configure(express.rest())
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({extended: true}))
  .use(function(req, res, next) {
    req.feathers.fromMiddleware = req.headers;
    next();
  });
  // Initialize our service with any options it requires
  app.use('/api/v1/saved-marker', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/v1/saved-marker');
  let colName = 'saved_marker';
  
  if(app.get('env')){
    if(app.get('env') == "production"){
      colName = "saved_marker"; 
    }
  }
  mongoClient.then(db => {
    service.Model = db.collection(colName);
  });

  service.hooks(hooks);
};
