const assert = require('assert');
const app = require('../../src/app');

describe('\'analyze-marker-content\' service', () => {
  it('registered the service', () => {
    const service = app.service('analyze-marker-content');

    assert.ok(service, 'Registered the service');
  });
});
