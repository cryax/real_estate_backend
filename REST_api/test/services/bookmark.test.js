const assert = require('assert');
const app = require('../../src/app');

describe('\'bookmark\' service', () => {
  it('registered the service', () => {
    const service = app.service('bookmark');

    assert.ok(service, 'Registered the service');
  });
});
