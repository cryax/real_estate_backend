const assert = require('assert');
const app = require('../../src/app');

describe('\'list-bookmark\' service', () => {
  it('registered the service', () => {
    const service = app.service('list-bookmark');

    assert.ok(service, 'Registered the service');
  });
});
