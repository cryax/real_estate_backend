const assert = require('assert');
const app = require('../../src/app');

describe('\'saved_marker\' service', () => {
  it('registered the service', () => {
    const service = app.service('saved-marker');

    assert.ok(service, 'Registered the service');
  });
});
